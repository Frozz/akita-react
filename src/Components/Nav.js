import React from 'react';
import './Components Styles/Nav.css';

function Nav() {

  return (
    <nav className="nav">
      <div className="container">
        <h1>Wilcza Łapa</h1>
        <div className="nav-links">
          <a href="#about">o nas</a>
          <a href="#offer">oferta</a>
          <a href="#" className="nav-contact-anchor" disabled="disabled">kontakt</a>
        </div>
      </div>
    </nav>
  )
}

export default Nav;


