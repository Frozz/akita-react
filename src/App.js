import React from 'react';
import Nav from './Components/Nav.js';
import LandingPage from './Components/LandingPage.js';
import About from './Components/About/About.js';
import Offer from './Components/Offer/Offer.js';
import Footer from './Components/Footer.js';
import './reset.css';
import './App.css';

function App() {
  return (
    <div>
      <Nav />
      <LandingPage />
      <About />
      <Offer />
      <Footer />
    </div>
  );
}

export default App;
